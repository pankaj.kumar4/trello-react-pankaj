import React, { Component } from 'react';
import { createNewCard } from './api';
class AddNewCard extends Component {
  state = {
    key: '868fb388da6cfa6fa7cd765182326010',
    token: '27ec8b765b6618d1bc05aed7624317369125720b78b62a124fdee0e8eb850045',
    input: '',
    cards: [],
    listId: ''
  };
  handleChange = event => {
    this.setState({ input: event.target.value });
  };
  async NewCard() {
    let card = await createNewCard(
      this.state.key,
      this.state.token,
      this.state.input,
      this.props.list.id
    );
    await this.props.updateCard(card);
  }
  addNewCard = e => {
    e.preventDefault();
    this.NewCard();
  };

  async componentDidMount() {
    await this.setState({
      cards: this.props.list.cards,
      listId: this.props.list.id
    });
  }
  render() {
    return (
      <form onSubmit={e => this.addNewCard(e)}>
        <input
          className="card"
          type="text"
          placeholder="...Add new card"
          onChange={this.handleChange}
        />
        <button className="btn btn-success" onClick={this.addNewCard}>
          Add
        </button>
      </form>
    );
  }
}

export default AddNewCard;
