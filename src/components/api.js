async function fetchBoards(key, token) {
  let boards = await fetch(
    `https://api.trello.com/1/members/pankajkumar305/boards?key=${key}&token=${token}`,
  );
  boards = await boards.json();
  return boards;
}
async function fetchLists(key, token, boardId) {
  let lists = await fetch(
    `https://api.trello.com/1/boards/${boardId}/lists?cards=all&card_fields=all&filter=open&fields=all&key=${key}&token=${token}`,
  );
  lists = await lists.json();
  return lists;
}
async function deleteRequest(key, token, cardId) {
  await fetch(
    `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`,
    { method: 'DELETE' },
  );
}
async function createNewCard(key, token, input, listId) {
  let card = await fetch(
    `https://api.trello.com/1/cards?name=${input}&idList=${listId}&keepFromSource=all&key=${key}&token=${token}`,
    { method: 'POST' },
  );
  card = await card.json();
  return card;
}
module.exports = {
  fetchBoards,
  fetchLists,
  deleteRequest,
  createNewCard,
};
