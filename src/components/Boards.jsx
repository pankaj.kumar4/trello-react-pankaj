import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class Boards extends Component {
  state = {
    key: '868fb388da6cfa6fa7cd765182326010',
    token: '27ec8b765b6618d1bc05aed7624317369125720b78b62a124fdee0e8eb850045'
  };
  boardStyle() {
    return {
      backgroundImage: 'url(' + this.props.board.prefs.backgroundImage + ')',
      backgroundSize: '20vh 10vh',
      color: 'white'
    };
  }

  render() {
    return (
      <Link
        to={{
          pathname: `board_id/${this.props.board.id}`,
          data: {
            id: this.props.board.id,
          }
        }}
      >
        <div style={this.boardStyle()} className="board">
          <h6>{this.props.board.name}</h6>
        </div>
      </Link>
    );
  }
}

export default Boards;
