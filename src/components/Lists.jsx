import React, { Component } from 'react';
import List from './List';
import { fetchLists } from './api';
class Lists extends Component {
  state = {
    key: '868fb388da6cfa6fa7cd765182326010',
    token: '27ec8b765b6618d1bc05aed7624317369125720b78b62a124fdee0e8eb850045',
    lists: [],
    board_id: ''
  };
  async componentDidMount() {
    await this.setState({
      board_id: this.props.match.params.id
    });
    let lists = await fetchLists(
      this.state.key,
      this.state.token,
      this.state.board_id
    );
    this.setState({ lists });
  }
  render() {
    return (
      <div className="lists">
        {this.state.lists.map(list => (
          <List key={list.id} list={list} />
        ))}
      </div>
    );
  }
}

export default Lists;
