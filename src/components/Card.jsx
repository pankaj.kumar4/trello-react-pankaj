import React, { Component } from 'react';
import {deleteRequest} from './api'

class Cards extends Component {
  state = {
    key: '868fb388da6cfa6fa7cd765182326010',
    token: '27ec8b765b6618d1bc05aed7624317369125720b78b62a124fdee0e8eb850045',
    card: {}
  };

  deleteCard = () => {
    deleteRequest(this.state.key,this.state.token,this.props.card.id);
    this.props.removeCard(this.state.card.id);
  };
  componentDidMount() {
    this.setState({ card: this.props.card });
  }
  render() {
    return (
      <div className="card">
        <p>{this.props.card.name}</p>
        <button className="btn btn-danger btn-sm" onClick={this.deleteCard}>
          X
        </button>
        
      </div>
    );
  }
}

export default Cards;
