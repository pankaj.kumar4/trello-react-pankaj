import React, { Component } from 'react';
import Card from './Card';
import AddNewCard from './AddNewCard';
class List extends Component {
  state = {
    list: [],
    cards: []
  };
  componentDidMount() {
    this.setState({ list: this.props.list, cards: this.props.list.cards });
  }
  updateCard = card => {
    let cards = [...this.state.cards];
    cards.push(card);
    this.setState({ cards: cards });
  };
  removeCard = cardId => {
    let updatedCards = this.state.cards.filter(card => card.id !== cardId);
    this.setState({cards: updatedCards})
  };
  render() {
    return (
      <div className="list">
        <h6>{this.state.list.name}</h6>
        {this.state.cards.map(card => (
          <Card key={card.id} card={card} removeCard={this.removeCard} />
        ))}
        <AddNewCard list={this.state.list} updateCard={this.updateCard} />
      </div>
    );
  }
}

export default List;
