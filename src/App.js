import Boards from './components/Boards';
import Lists from './components/Lists';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import React, { Component } from 'react';
import { fetchBoards } from './components/api';

class App extends Component {
  state = {
    key: '868fb388da6cfa6fa7cd765182326010',
    token: '27ec8b765b6618d1bc05aed7624317369125720b78b62a124fdee0e8eb850045',
    boards: [],
  };

  async componentDidMount() {
    const boards = await fetchBoards(this.state.key, this.state.token);
    this.setState({ boards });
  }
  render() {
    return (
      <Router>
        <Route
          exact
          path="/"
          render={props => (
            <div className="App">
              <header>Trello</header>
              <div className="boards">
                {this.state.boards.map(board => (
                  <Boards key={board.id} board={board} />
                ))}
              </div>
            </div>
          )}
        />
        <Route path="/board_id/:id" component={Lists} />
      </Router>
    );
  }
}

export default App;
